package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String hello() {
        System.out.println("Hello world!");
        return "Hello world!";
    }

    @RequestMapping(path = "/work", method = RequestMethod.GET)
    public String work() {
        System.out.println("Work");
        return "Work!";
    }

    @RequestMapping(path = "/bye", method = RequestMethod.GET)
    public String bye() {
        System.out.println("Bye!");
        return "Bye!";
    }
}
